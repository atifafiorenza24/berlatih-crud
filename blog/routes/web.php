<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', 'HomeController@dashboard');
Route::get('/pendaftaran', 'AuthController@daftar');
Route::post('/kirim', 'AuthController@kirim');

Route::get('/data-table', function(){
    return view('halaman.data-table');
});

Route::get('/master', function(){
    return view('layout.master');
});

//create
//form input data create cast
Route::get('/cast/create', 'CastController@create');
//form post menyimpan data ke database
Route::post('/cast', 'CastController@store');

//read
//menampilkan semua data di table cast
Route::get('/cast', 'CastController@index');
//menampilkan data sesuai id
Route::get('/cast/{cast_id}', 'CastController@show');

//update
//form edit data
Route::get('/cast/{cast_id}/edit', 'CastController@edit');
//update data berdasarkan id
Route::put('/cast/{cast_id}', 'CastController@update');

//delete data
Route::delete('/cast/{cast_id}', 'CastController@destroy');
