@extends('layout.master')
@section('judul')
Halaman Data Cast
@endsection

@section('content')
<a href="/cast/create" class="btn btn-primary btn-sm my-2">Tambah Data Cast</a>

<table class="table table-bordered table-striped">
    <thead>
    <tr>
        <th>No.</th>
        <th>Nama</th>
        <th>Umur</th>
        <th>Bio</th>
        <th>Action</th>
    </tr>
    </thead>
    <tbody>
        @forelse ($cast as $key=>$item)
        <tr>
            <th>{{$key+1}}</th>
            <td>{{$item->nama}}</td>
            <td>{{$item->umur}}</td>
            <td>{{$item->bio}}</td>
            <td>
                <form action="/cast/{{$item->id}}" method="POST">
                    @csrf
                    @method('delete')
                    <a href="/cast/{{$item->id}}" class="btn btn-primary btn-sm btn-info">Details</a>
                <a href="/cast/{{$item->id}}/edit" class="btn btn-primary btn-sm btn-warning">Edit</a>
                    <input type="submit" class="btn btn-sm btn-danger" value="Delete">
                </form>
            </td>
        </tr>
        @empty
        <tr>
            <td>Data Kategori Kosong</td>
        </tr>
        @endforelse
    </tbody>
  </table>
@endsection