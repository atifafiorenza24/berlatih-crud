@extends('layout.master')
@section('judul')
Halaman Edit Cast
@endsection

@section('content')

<form action="/cast/{{$cast->id}}" method="POST">
    @csrf
    @method('put')
    <div class="form-group">
      <label>Nama</label>
      <input type="text" value="{{$cast->nama}}" class="form-control" name="nama" placeholder="Enter nama">
    </div>
    @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
      <label>Umur</label>
      <input type="text" value="{{$cast->umur}}" class="form-control" name="umur"  placeholder="Enter age">
    </div>
    @error('umur')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    
    <div class="form-group">
        <label>Bio</label>
        <textarea class="form-control" name="bio" cols="30" rows="10" placeholder="Enter bio">{{$cast->bio}}</textarea>
    </div>
    @error('bio')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <button type="submit" class="btn btn-primary">Submit</button>
</form>

@endsection