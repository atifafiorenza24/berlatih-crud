{{-- <!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="with=device-width, initial-scale=1.0">
    <title>Form</title>
</head> --}}
@extends('layout.master')
@section('judul')
<h2>Sign Up Form</h2>
Buat Account Baru!
@endsection
@section('content')

<div class="form-group">
    <form action="/kirim" method="post">
        <div>
            <label>First Name</label> <br><br>
            <input type="text" name="firstname">
        </div>
        <div>
            <br><label>Last Name</label> <br><br>
            <input type="text" name="lastname">
        </div>
    </form>
</div>

<p>Gender:</p>
<input type="radio" name="gender" value="male"> Male<br>
<input type="radio" name="gender" value="female"> Female<br>
<input type="radio" name="gender" value="other"> Other

<p>Nationality:</p>
<form>  
    <select>  
        <option value="indonesian">Indonesia</option>  
    </select>  
 </form>  

<p>Language Spoken:</p>
<input type="checkbox" name="language" value="indonesia">Bahasa Indonesia<br>
<input type="checkbox" name="language" value="english">English<br>
<input type="checkbox" name="language" value="other"> Other

<div class="form-group">
    <br><label>Bio: </label> <br><br>
    <textarea cols="30" rows="10"></textarea>
</div>

<a href="/kirim">
    <button type="submit" >Sign Up</button>
</a>
@endsection
